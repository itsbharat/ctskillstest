<div class="container">
	@if( count($products) > 0 )
		<table id="product-table" class="table">
		    <thead>
		        <tr>
		            <th>Name</th>
		            <th>Price</th>
		            <th>Quantity</th>
		            <th>Total Value Number</th>
		            <th>Submitted At</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php $total = 0; ?>
		    	@foreach($products as $product)
		        <tr>
		            <td> {{ $product['name'] }} </td>
		            <td> {{ $product['price'] }} </td>
		            <td> {{ $product['quantity'] }} </td>
		            <td> {{ $product['quantity'] * $product['price'] }} </td>
		            <?php $total +=  $product['quantity'] * $product['price']; ?>
		            <td> {{ $product['created_at'] }} </td>
		        </tr>
		        @endforeach

		        <tfoot>
		            <td></td>
		            <td></td>
		            <td>Total</td>
		            <td> {{ $total }}</td>
		            <td></td>
		        </tr>

		    </tbody>
		</table>
	@else
		<div class="row center">
			<h2>Add some products</h2>
		</div>
	@endif
</div>