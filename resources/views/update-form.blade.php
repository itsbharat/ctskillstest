<div class="container">
	<div class="row">
		<h2>Add New Product</h2>
	</div>
    {!! Form::open(array( 'url' =>'/product', 'id'=>'productForm')) !!}
        
        {{ csrf_field() }}

        <div class="row">
                <div class="col-md-10 col-sm-10">
                    <div class="form-group form-group-sm">
                        <label for="firstname" class="control-label">Product Name</label>
                        <input type="text" class="form-control" name="name" id="product-name" placeholder="Product Name" required>
                    </div>
                </div>

                <div class="col-md-10 col-sm-10">
                    <div class="form-group form-group-sm">
                        <label for="quantity" class="control-label">Quantity in Stock</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" placeholder="Quantity" required min="0">
                    </div>
                </div>
                <div class="col-md-10 col-sm-10">
                    <div class="form-group form-group-sm">
                        <label for="price" class="control-label">Product Price</label>
                        <input type="number" class="form-control" step="0.01" name="price" id="price" placeholder="Price" min="0.01">
                    </div>
                </div>
        </div>
        <div class="row">
             <div class="col-xs-3">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
        </div>
    {{ Form::close() }}

	</br>

    @include('errors')
</div>