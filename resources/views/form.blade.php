<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Coaliton Technologies Skill Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!--Include Bootstrap-->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">


        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

        <!-- For Datatable -->
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.19/api/sum().js "></script>

    </head>
    <body>
        <div>
            <div class="content">
                    @include('update-form')
            </div>

            <div class="content">
                <div class="title m-b-md">
                    @include('table')
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready( function () {
                $('#product-table').DataTable({
                    paging:false,
                    order: [[ 4, "desc" ]],
                });
            } );
        </script>

    </body>
</html>
