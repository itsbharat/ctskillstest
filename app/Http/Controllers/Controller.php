<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Requests\AddNewProductRequest;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getForm()
    {
    	$products = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json'), true) : [];
    	return view('form')->withProducts($products);
    }

    public function storeProduct(AddNewProductRequest $request)
    {
        try {
            //
            if(Storage::disk('local')->exists('products.json'))
            {
            	$products = json_decode(Storage::disk('local')->get('products.json'), true);
            }
            else {
            	$products = [];
            };
            $product = $request->only(['name', 'quantity', 'price']);
            $product['created_at'] = date('Y-m-d H:i:s');
            array_push($products, $product);
            Storage::disk('local')->put('products.json', json_encode($products));
            return redirect('/');
        } catch(Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()]; 
        }
    }
}
