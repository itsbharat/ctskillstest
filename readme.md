# Libraries, Packages and Frameworks used

  - Laravel
  - Laravel Form Package
  - Bootstrap
  - JQuery
  - Datatables


Steps to Run:
  - Download or clone the project
  - Install packages and dependencies using composer install
  - Run using php artisan serve

P.S. The editing part can be completed using the paid editor API of datatables or custom code (but I guess timelimit)